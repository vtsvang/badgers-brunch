HomeController = require 'controllers/home-controller'
NotificationsCollection = require 'models/notifications'
NotificationsView = require 'views/notifications-index-view'

module.exports = class NotificationsController extends HomeController

  limit: 2

  index: ( params ) ->
    @collection = new NotificationsCollection()
    @view = new NotificationsView collection: @collection

    fetch = @collection.fetch
      data:
        limit: @limit
        offset: @limit * ( ( params.page ? 1 ) - 1 )


    fetch.then =>
      @view.render()