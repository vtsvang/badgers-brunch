CollectionView = require 'views/base/collection-view'
NotificationsItem = require 'views/notifications-item-view'
PaginationView = require 'views/pagination-view'

module.exports = class NotificationsIndexesView extends CollectionView
  itemView: NotificationsItem
  region: 'main'
  animationDuration: 0
  listSelector: '> table > tbody'
  template: require './templates/notifications-index'
  autoRender: true

  regions:
    pagination: '.js-pagination'

  render: ->
    super
    @pagination = new PaginationView model: @collection
    @