View = require 'views/base/view'

module.exports = class NotificationsItemView extends View
  template: require './templates/notifications-item'
  tagName: 'tr'
