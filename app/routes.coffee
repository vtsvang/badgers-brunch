module.exports = (match) ->
  match '', 'home#index'
  match 'notifications', 'notifications#index'
  match 'notifications/:page', 'notifications#index'
