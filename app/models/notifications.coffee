Collection = require 'models/base/collection'
Notifications = require './notifications'

module.exports = class Notifications extends Collection
  model: Chaplin.Model

  url: "/api/notifications.json"
  parse: ( res ) ->
    @total = res.total
    @offset = res.offset
    @limit = res.limit

    res.items
