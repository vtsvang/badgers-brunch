View = require 'views/base/view'

module.exports = class PaginationView extends View
  template: require './templates/pagination'
  region: 'pagination'
  autoRender: true

  getTemplateData: ->
    pagination:
      total: @model.total
      offset: @model.offset
      limit: @model.limit
